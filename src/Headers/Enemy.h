#pragma once
#include <ncurses.h>
#include <vector>
#include <string>
#include "Player.h"
#include "Pos.h"
#include "Cell.h"

class Player; 

/*! 
	\brief Class of enemy.

*/

class Enemy : public Object{
protected:
    int health, damage, endurance;
    int radius;///< Radius of vision.
    int exp;///< How much experience player will get.

public:
    Enemy(Pos p, char ch);

    /*!
    	\brief Make move.
    	\param map The map in which will move.
    	\param pl Pointer to player.
    */
    void MakeMove(vector<vector<Cell>> & map, Player * pl);

    int TakeDamage(int takeDamage);

    int GetHealth();

    virtual void Print(WINDOW * win);
};