#pragma once
#include <ncurses.h>
#include <string>
#include <vector>
#include <list>
#include "Object.h"
#include "Constants.h"
#include <map>
#include "Enemy.h"
#include "Cell.h"
#include "Thing.h"

/*! 
    \brief Player's class

*/

class Player : public Object{
    map <string,int> characteristics;///< General characteristics.
    int direction;///< Player's direction.
    WINDOW * win;///< Print to this window.
    vector<vector<Cell>> * curMap;///< Actual map. 
    list<string> * text;///< Log, which is visible in the UI window.
    int health, experience, nextLevel; 
    int curEqpt; ///< Current equipments

/*!
    \brief Attack enemy in the front.
*/
    bool Attack();

/*!
    \brief Add new thing to inventory.
*/
    void AddThing(Thing * t);

/*!
    \brief Regenerate health.
*/
    void Regen();

public:
    vector<Thing> inv;
    Player(map <string,int> characteristics);

/*!
    \brief Change characteristic.
    \param name Name of characteristic.
    \param value Value of increase.
*/
    void ChangeCharac(string name, int value);

/*!
    \brief Make turn.
*/
    bool MakeMove(char in);

/*!
    \brief Go to position.
    \param x, y Coordinates.
*/
    void GoTo(int x, int y);

    void TakeDamage(int takeDamage);

    void Print(WINDOW * win);

    int GetStats(string key);
    int GetHealth() const;
    int GetDamage() const;
    int GetDefence() const;
    int GetExp() const;
    int GetCurEqpt() const;
    int GetNextLevelExp() const;
    vector<Thing*> GetInv();

    void SetExp(int exp);
    void SetNextLevelExp(int nlexp);
    void SetHealth(int hlth);
    void SetCurEqpt(int ceqpt);

/*!
    \brief Take experience.
*/
    void TakeExp(int exp);

/*!
    \brief Set curcent map, window for output and list with logs.
*/
    void SetWinAndEnem(WINDOW * win, vector<vector<Cell>> * curMap, list<string> * text);

    void Rotate(char in);

    ~Player();
};