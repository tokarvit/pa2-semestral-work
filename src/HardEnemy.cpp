#include <ncurses.h>
#include "Headers/Pos.h"
#include "Headers/HardEnemy.h"

HardEnemy::HardEnemy (Pos p, char ch) : Enemy(p, ch){
	health		= 80;
	damage		= 22;
	endurance	= 3;
	radius		= 9;
	exp			= 100;
}

void HardEnemy::Print(WINDOW * win){
	wattron(win,COLOR_PAIR(COLOR_RED) | A_BOLD);
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(COLOR_RED) | A_BOLD);
}