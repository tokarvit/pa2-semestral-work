#pragma once
#include <ncurses.h>
#include "Pos.h"
#include "Enemy.h"

/*! 
	\brief Class of hard enemy.

*/

class HardEnemy : public Enemy{
public:
	HardEnemy (Pos p, char ch);

	void Print(WINDOW * win);
};