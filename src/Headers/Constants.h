#pragma once
#include <string>

using namespace std;

//Size of screen
#define GAME_WIDTH 112
#define GAME_HEIGHT 30
#define GUI_WIDTH 32
#define HELP_WIDTH 40
#define HELP_HEIGHT 11

//Place to output image
#define LEFT_TOP 1
#define LEFT_MIDDLE 4
#define LEFT_BOTTOM 7

#define MIDDLE_TOP 2
#define MIDDLE_MIDDLE 5
#define MIDDLE_BOTTOM 8

#define RIGHT_TOP 3
#define RIGHT_MIDDLE 6
#define RIGHT_BOTTOM 9

//Directions of player
#define LEFT '<'
#define UP  '^'
#define RIGHT '>'
#define DOWN 'v'

//Objects reactions
#define NOEFFECT 0
#define NOTRESPASSING 1
#define NEXTMAP 2
#define THING 3

//Return values
#define CONTINUE 0
#define EXIT 1
#define NEXTMOVE 2

static string RES_PATH =  "examples/";

//Define colors names
#define SAND 	8
#define WATER 	9
#define PLAYER 	10
#define STEM 	11
#define TEXTW 	12
#define TEXTY 	13
#define TEXTR 	14
#define TEXTG 	15
#define TEXTB 	16
#define TEXTM 	17
#define TREE	18
#define THINGC	19


