#include <ncurses.h>
#include <map>
#include <string>
#include <vector>
#include <list>
#include <fstream>
#include "Headers/Constants.h"
#include "Headers/Resources.h"
#include "Headers/Func.h"



Resources::Resources(){
    Log("Get resources..");

    //Get all resouces's path
	list<string> listOfResFiles;
    ifstream resList;
    resList.open(RES_PATH + "resList.txt"); //resList.txt - all resources path file
    if(resList.is_open()){
        string line;
        while ( getline (resList,line) ){
            if(line.back() == '\n')
                line.pop_back();
            listOfResFiles.push_back(line);
        }   
        resList.close();
    } else {
        throw make_pair("FileError","Cant open resList.txt.");
        return;
    }

    //Read all resources from resList
    for(auto it = listOfResFiles.begin(); it != listOfResFiles.end(); it++){
        ifstream file;
        file.open(RES_PATH + *it);
        if(file.is_open()){
            string line;
            vector <string> vec;
            while ( getline (file,line) ){
                vec.push_back(line);
            } 
            string nameRes = *it;
            nameRes.erase(nameRes.size() - 4,4);
            allres[nameRes] = vec;
            file.close();
        } else {
            throw make_pair("FileError", RES_PATH + *it);
            return;
        }
    }

    //Read all level which can find
    for(lastMap = 1; true ; lastMap++){
        ifstream file;
        string nameMap = "map_" + to_string(lastMap);
        file.open(RES_PATH + nameMap);
        if(file.is_open()){
            string line;
            vector <string> vec;
            while ( getline (file,line) ){
                vec.push_back(line);
            }
            allres[nameMap] = vec;
            file.close();
        } else {
            if(lastMap == 1)
                throw make_pair("Does not find any map" , "!");
            break;
        }
    }
    
    Log("Getting resources COMPLETE.");
}

void Resources::PrintImg(WINDOW * win, string key, int pos){
    int h,w;
    getmaxyx(win, h, w);
    if(allres.count(key) == 0)
        throw "Key " + key + " does not exist!";

    for(int y = 0; y < (int)allres[key].size(); y++){
        for(int x = 0; x < (int)allres[key][y].size(); x++){
            int cl;

            //Choose color bae on symbol
            switch(allres[key][y][x]){
                case 'R':
                    cl = 1;
                    break;
                case 'G':
                    cl = 2;
                    break;
                case 'Y':
                    cl = 3;
                    break;
                case 'B':
                    cl = 4;
                    break;
                case 'M':
                    cl = 5;
                    break;
                case 'C':
                    cl = 6;
                    break;
                case 'W':
                    cl = 7;
                    break;
                default:
                    cl = 0;
                    break;
            }
            wattron(win,COLOR_PAIR(cl));

            //Move to position
            switch(pos){
                case LEFT_TOP:
                    wmove(win, y + 2, x);
                    break;
                case MIDDLE_TOP:
                    wmove(win, y + 2, x + w/2 - (int)allres[key][1].size()/2);
                    break;
                case RIGHT_TOP:
                    wmove(win, y + 2, x + w - (int)allres[key][1].size()/2 - 1);
                    break;
            }

            waddch(win, ' ');
            wattroff(win,COLOR_PAIR(cl));
        }
    }

    wrefresh(win);
    w=h; //Duct tape to avoid warning in complilator
}


vector<string> Resources::GetMap(string key){
    if(allres.count(key) == 0)
        Log("Key not found " + key + ".");
    return allres[key];
}