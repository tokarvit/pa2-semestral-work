#pragma once

#include <ncurses.h>
#include <string>
#include <vector>
#include <map>
#include <list>
#include "Constants.h"
#include <fstream>


/*! 
	\brief Class of all resources.

*/


class Resources{
public:
	int lastMap; //< Level amount.

	map <string,vector <string>> allres; //< All resources.

/*!
	\brief During the creation read all maps and another resources from text file resList.txt.
*/
	Resources();

/*!
	\brief Show image on window.
	\param win Window for showing . 
	\param key Key of image.
	\param pos Image's position. 
*/
	void PrintImg(WINDOW * win, string key, int pos);

	vector<string> GetMap(string key);
};