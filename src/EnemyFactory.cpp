#include "Headers/Enemy.h"
#include "Headers/EasyEnemy.h"
#include "Headers/AverageEnemy.h"
#include "Headers/HardEnemy.h"
#include "Headers/EnemyFactory.h"
#include "Headers/Constants.h"




Enemy * EnemyFactory::NewEnemy(Pos p, char ch){
	if(ch == '1')
		return new EasyEnemy(p, ch);
	if(ch == '2')
		return new AverageEnemy(p, ch);
	if(ch == '3')
		return new HardEnemy(p, ch);

	return NULL;
}