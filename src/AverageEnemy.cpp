#include "Headers/Constants.h"
#include "Headers/AverageEnemy.h"
#include <ncurses.h>

AverageEnemy::AverageEnemy (Pos p, char ch) : Enemy(p, ch){
	health		= 50;
	damage		= 25;
	endurance	= 2;
	radius		= 6;
	exp			= 70;
}

void AverageEnemy::Print(WINDOW * win){
	wattron(win,COLOR_PAIR(COLOR_BLUE) | A_BOLD);
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(COLOR_BLUE) | A_BOLD);
}