#include <string>
#include <vector>
#include <map>
#include <fstream>
#include "Headers/Saving.h"
#include "Headers/Player.h"
#include "Headers/Func.h"


Saving::Saving(){
    currentLevel = 1;
    player = NULL;
}

bool Saving::Load(){
	ifstream f ("saving", ios::binary);
	if(!f){
		Log("Cannot open save file.");
		return false;
	}
	map <string,int> cht;

    //Load level depended characteristics
	int16_t t;
	f.read((char *)&t, sizeof(t));
	currentLevel = t;

	f.read((char *)&t, sizeof(t));
	cht["health"] = t;
	
	f.read((char *)&t, sizeof(t));
    cht["damage"] = t;
	
	f.read((char *)&t, sizeof(t));
    cht["defence"] = t;
	
	f.read((char *)&t, sizeof(t));
    cht["regeneration"] = t;

    if(player)
        delete player;
    player = new Player(cht);

    //Load game depended characteristics
    f.read((char *)&t, sizeof(t));
	player->SetHealth(t);

    f.read((char *)&t, sizeof(t));
	player->SetExp(t);
    
    f.read((char *)&t, sizeof(t));
	player->SetNextLevelExp(t);

    f.read((char *)&t, sizeof(t));
    player->SetCurEqpt(t);


    //Load inventory
    player->inv.clear();
    while(true){
        //Length of thing's name
        f.read((char *)&t, sizeof(t));
        if(f.eof())
            break;
        int nameSize = (int)t;

        //Read thing's name
        char * buffer = new char[nameSize];
        f.read(buffer, nameSize);

        f.read((char *)&t, sizeof(t));
        int dam = t;

        f.read((char *)&t, sizeof(t));
        int def = t;

        f.read((char *)&t, sizeof(t));
        char ch = (char)t;

        //Add this thing to inventory
        player->inv.push_back(Thing(Pos(), ch, dam, def, string(buffer,nameSize)));

        delete [] buffer;
    }


	return true;
}

void Saving::NewSaving(){
	delete player;
    player = NULL;
	currentLevel = 1;
	
	map <string,int> cht;
    string choiced;
    int curYPos = 1; // Line position
    int leftIndent = 3;

    WINDOW * menuWin = newwin(GAME_HEIGHT,GAME_WIDTH, 0,0);
    MakeBox(menuWin);
    wrefresh(menuWin);
    int r, c, h, w;

    //Make stories's window
    getmaxyx(menuWin, r, c);
    h = r - 10;
    w = c - c/5;
    WINDOW * storyWin = newwin(h, w, 2, (c - w)/2);

    MakeBox(storyWin);

    wrefresh(storyWin);

    WINDOW * selectWin = newwin(h/3, c/3, h + 3, (c - c/3)/2);

    //Choose name
    mvwprintw(storyWin, curYPos++,leftIndent, 
        "Hi! My name is ");
    wrefresh(storyWin);
    vector <string> choices = {"Bob", "Jack", "Sam", "Chris"};
    wprintw(storyWin, SelectMenu(selectWin, choices, 0).c_str());
    wprintw(storyWin, ".");
    wrefresh(storyWin);
    werase(selectWin);
    wrefresh(selectWin);

    //Choose nation
    mvwprintw(storyWin, curYPos++,leftIndent, 
        "I want to tell you my story. I grew up in ");
    wrefresh(storyWin);
    choices = {"Norway", "Egypt", "France"};
    choiced = SelectMenu(selectWin, choices,0);
    wprintw(storyWin, choiced.c_str());
    if(choiced == "Norway"){
            wprintw(storyWin, ". Our people have good ");
            mvwprintw(storyWin, curYPos++,leftIndent, 
                "health and live to 90 years old.");

            cht["health"] = 60;
            cht["damage"] = 5;
            cht["defence"] = 5;
            cht["regeneration"] = 5;
    } else if(choiced == "Egypt"){
            wprintw(storyWin, ". As you know Egyptians ");
            mvwprintw(storyWin, curYPos++,leftIndent, 
                "are fast and have good strength.");

            cht["health"] =40;
            cht["damage"] = 8;
            cht["defence"] =10;
            cht["regeneration"] = 3;
    } else if(choiced == "France"){
            wprintw(storyWin, ". We are nation with ");
            mvwprintw(storyWin, curYPos++,leftIndent, 
                "great history. And through our defence we have a lot of wins.");

            cht["health"] =50;
            cht["damage"] = 7;
            cht["defence"] =6;
            cht["regeneration"] = 10;
    }
    wrefresh(storyWin);
    werase(selectWin);
    wrefresh(selectWin);


    //Choose specilization
    mvwprintw(storyWin, curYPos++,leftIndent, 
        "All my life I was a ");
    choices = {"Solder", "Thief", "Monk"};
    choiced = SelectMenu(selectWin, choices,0);
    wprintw(storyWin, choiced.c_str());
    wprintw(storyWin, ".");
    wrefresh(storyWin);
    if(choiced == "Solder"){
            cht["health"] += 20;
            cht["damage"] += 5;
            cht["defence"] += 5;
    } else if(choiced == "Thief"){
            cht["health"] += 5;
            cht["damage"] += 15;
            cht["defence"] += 3;
            cht["regeneration"] += 5;
    } else if(choiced == "Monk"){
            cht["health"] += 40;
            cht["damage"] += 3;
            cht["defence"] += 3;
            cht["regeneration"] = 10;
    }

    //A little bit more story
    mvwprintw(storyWin, curYPos++,leftIndent, 
            "Once the war started, I served on the ship. Everything was good for a while.");
    mvwprintw(storyWin, curYPos++,leftIndent, 
            "And in one moment everything has been changing. ");
    mvwprintw(storyWin, curYPos++,leftIndent, 
            "I woke up on the island, surrounded by enemy...");
    wrefresh(storyWin);

    //Wait player agree to start game   
    werase(selectWin);
    mvwprintw(selectWin, 2, 5 , "Press any key to continue");
    wrefresh(selectWin);
    getch();

    //Delete all waste window
    werase(storyWin);
    werase(selectWin);
    werase(menuWin);
    wrefresh(storyWin);
    wrefresh(selectWin);
    wrefresh(menuWin);
    delwin(storyWin);
    delwin(selectWin);
    delwin(menuWin);

    //Create player with this parametr
    player = new Player(cht);
    SaveToDisk();
}

bool Saving::SaveToDisk(){
	ofstream f ("saving", ios::binary | ios::trunc);
	if(!f){
		Log("Cannot create save file.");
		return false;
	}

    //Write current level
	int16_t tm = (int16_t)currentLevel;
    f.write((const char *) & tm, sizeof(tm));

    //Write level depends characteristics
    tm = (int16_t)player->GetStats("health");
    f.write((const char *) & tm, sizeof(tm));
	
	tm = (int16_t)player->GetStats("damage");
    f.write((const char *) & tm, sizeof(tm));
	
	tm = (int16_t)player->GetStats("defence");
   	f.write((const char *) & tm, sizeof(tm));

	tm = (int16_t)player->GetStats("regeneration");
 	f.write((const char *) & tm, sizeof(tm));
	
    //Write game depends characteristics
	tm = (int16_t)player->GetHealth();
    f.write((const char *) & tm, sizeof(tm));

	tm = (int16_t)player->GetExp();
    f.write((const char *) & tm, sizeof(tm));

	tm = (int16_t)player->GetNextLevelExp();
    f.write((const char *) & tm, sizeof(tm));

	tm = (int16_t)player->GetCurEqpt();
    f.write((const char *) & tm, sizeof(tm));

    //Write all things from inventory
    for(auto i: player->inv){
        //Name length
        tm = (int16_t)i.name.size();
        f.write((const char *) & tm, sizeof(tm));

        //Name
        f.write(i.name.c_str(), i.name.size()); 
        
        tm = (int16_t)i.damage;
        f.write((const char *) & tm, sizeof(tm));

        tm = (int16_t)i.defence;
        f.write((const char *) & tm, sizeof(tm));

        tm = (int16_t)i.GetSym();
        f.write((const char *) & tm, sizeof(tm));
    }

	f.close();
    return true;
}

Saving::~Saving(){
	delete player;
}