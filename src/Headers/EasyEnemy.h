#pragma once
#include <ncurses.h>
#include "Pos.h"
#include "Enemy.h"

/*! 
	\brief Class of average enemy.

*/

class EasyEnemy : public Enemy{
public:
	EasyEnemy (Pos p, char ch);

	void Print(WINDOW * win);
};