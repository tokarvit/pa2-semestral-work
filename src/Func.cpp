#include <string>
#include <vector>
#include <fstream>
#include <ncurses.h>
#include "Headers/Constants.h"

void Log(string s){
	ofstream l;
	l.open("log.txt", fstream::app);
	if(!l)
		l.open("log.txt", fstream::app | fstream::trunc);
	l << s << endl;
	l.close();
}

string SelectMenu(WINDOW * win, const vector<string> & choices, int indent){
    int highlight = 0;
    int choice = 0;
    int choicesize = choices.size();
    int w,h;
    getmaxyx(win, h, w);

    while(true){
        //Print varients to choose
        for (int i = 0; i < choicesize; i++)
        {
            if(i == highlight)
                wattron(win, A_REVERSE);
            mvwprintw(win, i + indent, w/2 - choices[i].size()/2, choices[i].c_str());
            wattroff(win, A_REVERSE);
            Log(choices[i]);
        }
        wrefresh(win);

        //Process input
        choice = wgetch(win);
        switch(choice){
            case 'w':
                highlight--;
                if(highlight == -1)
                    highlight = 0;
                break;
            case 's':
                highlight++;
                if(highlight == choicesize)
                    highlight--;
                break;
            default:
                break;
        }
        if(choice == 10)
            break;
    }

    w=h; //Duct tape to avoid warning in complilator
    return choices[highlight]; 
}

void InitColors(){
    start_color();
    init_pair(TREE, COLOR_BLACK, COLOR_YELLOW);
    init_pair(COLOR_RED, COLOR_RED, COLOR_RED);
    init_pair(COLOR_GREEN, COLOR_GREEN, COLOR_GREEN);
    init_pair(COLOR_YELLOW, COLOR_YELLOW, COLOR_YELLOW);
    init_pair(COLOR_BLUE, COLOR_BLUE, COLOR_BLUE);
    init_pair(COLOR_MAGENTA, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(COLOR_CYAN, COLOR_CYAN, COLOR_CYAN);
    init_pair(COLOR_WHITE, COLOR_WHITE, COLOR_WHITE);
    init_pair(SAND, COLOR_YELLOW, COLOR_YELLOW);
    init_pair(WATER, COLOR_WHITE, COLOR_CYAN);
    init_pair(PLAYER, COLOR_RED, COLOR_WHITE);
    init_pair(STEM, COLOR_BLACK, COLOR_BLACK);
    init_pair(TEXTW, COLOR_WHITE, COLOR_BLACK);
    init_pair(TEXTY, COLOR_YELLOW, COLOR_BLACK);
    init_pair(TEXTR, COLOR_RED, COLOR_BLACK);
    init_pair(TEXTG, COLOR_GREEN, COLOR_BLACK);
    init_pair(TEXTB, COLOR_BLUE, COLOR_BLACK);
    init_pair(TEXTM, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(THINGC, COLOR_WHITE, COLOR_YELLOW);
}

void MakeBox(WINDOW * win){
    wattron(win, COLOR_PAIR(COLOR_WHITE));
    box(win, 0 , 0);  
    wattroff(win, COLOR_PAIR(COLOR_WHITE));
}