#pragma once

#include <map>
#include <string>
#include <vector>
#include <ncurses.h>

/*! 
	\brief The file with utility functions.

*/

string SelectMenu(WINDOW * win, const vector<string> & choices, int indent);

void InitColors();

//Game log to file
void Log(string s);

//Make border around window
void MakeBox(WINDOW * win);