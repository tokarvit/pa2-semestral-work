#include <vector>
#include <memory>
#include <string>
#include <ncurses.h>
#include "Headers/Player.h"
#include "Headers/Func.h"
#include "Headers/Saving.h"
#include "Headers/Enemy.h"
#include "Headers/Resources.h"
#include "Headers/Game.h"
#include "Headers/Constants.h"

/*! \mainpage RPG Game
 *
 * \section intro_sec Description
 *
 * This game was made as semestral work with subject PA2 in the year 2017.
 
 * The main character was in the army and fought with enemy of him nation.\n
 * One day he had woke up and seen a lot of enemies around him. He was on \n
 * the island. The target is get out from it. The various of thing on this \n 
 * island can help you with this. 
 * 
 * 
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Open folder with game.
 * \subsection step2 Step 2: Run command "make compile".
 * \subsection step3 Step 3: Run command "make run".
 *
 */

using namespace std;


int main() {
    initscr();			
    raw();				
    keypad(stdscr, TRUE);	
    noecho();			
    curs_set(0);
    InitColors();
    Log("\n\nRunning game");

    //Check window size
    while(true){
        int y,x;
        getmaxyx(stdscr, y, x);
        if(y >= GAME_HEIGHT && x >= GAME_WIDTH)
            break;
        erase();
        mvprintw(y/2 , x/2 - 15, "Window too small, please expand.");
        refresh();
        getch();
    }
    erase();

    //Load resources
	Resources * res;
    try{
        res = new Resources();
    }
    catch (pair <char const*,string> p){
        endwin();
        Log(p.second);
        return 1;
    }
	refresh();

    //Start game
    Game game(res);
    while(game.CallMenu()){
   		game.Play();
   	}
    

    Log("Exit from game"); 
    delete res;
    endwin();
    return 0;
}
