#pragma once
#include <cmath>
#include "Func.h"
#include "Pos.h"
#include "Object.h"

/*! 
    \brief Thing for player.
*/
class Thing : public Object{
public :
	string name;	///< Thing's name.
	int damage; 	///< Increase damage.
	int defence;	///< Increase defence.

	Thing(Pos p, char ch, int dam, int def, string name = "Clothes");

	void Print(WINDOW * win);

/*! 
    \brief Create new thing.
*/
	static Thing * NewThing(Pos p, char ch);
};