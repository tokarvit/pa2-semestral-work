#include <ncurses.h>
#include <string>
#include <vector>
#include <map>
#include "Headers/Object.h"
#include "Headers/Player.h"
#include "Headers/Enemy.h"
#include "Headers/Func.h"
#include "Headers/Constants.h"
#include "Headers/Pos.h"
#include "Headers/Cell.h"


Player::Player(map <string,int> characteristics) : Object(Pos(0,0),UP){
    this->characteristics = characteristics;
    health = characteristics["health"];
    inv.push_back(Thing(Pos(0,0), 'x', 2, 2)); // start equipment
    curEqpt = 0;
    experience = 0;
    nextLevel = 30;
}

void Player::ChangeCharac(string name, int value){
    if(characteristics.count(name) == 0)
        return;
    characteristics[name] += value;
    if(characteristics[name] < 0)
        characteristics[name] = 0;
}

void Player::AddThing(Thing * t){
    inv.push_back(*t);
}

bool Player::MakeMove(char in){
    //Regenaration if less then half of health
    if(health < characteristics["health"]/2){
        health += characteristics["regeneration"];
        if(health > characteristics["health"]/2)
            health = characteristics["health"]/2;
    }

    //Processing inputs
    if(in == 'w' || in == 's'){
        int x, y;
        if(ch == DOWN){
            x = pos.x;
            if(in == 'w')
                y = pos.y + 1;
            else
                y = pos.y - 1;
        }  else if(ch == UP){
            x = pos.x;
            if(in == 'w')
                y = pos.y - 1;
            else
                y = pos.y + 1;
        } else if(ch == LEFT){
            y = pos.y;
            if(in == 'w')
                x = pos.x - 1;
            else
                x = pos.x + 1;
        } else if(ch == RIGHT){
            y = pos.y;
            if(in == 'w')
                x = pos.x + 1;
            else
                x = pos.x - 1;
        }
        if(!(*curMap)[y][x].CanMoveHere())
            return false;

        (*curMap)[pos.y][pos.x].Print(win);
        GoTo(x, y);
        Print(win);

        //If standing on thing then take it
        if((*curMap)[pos.y][pos.x].m_th != NULL){
            AddThing((*curMap)[pos.y][pos.x].m_th.get());
            (*curMap)[pos.y][pos.x].m_th.reset();
            text->push_back("You find new thing.");
        }
    } 
    else if(in == ' '){
        Attack();
    }

    return true;
}

void Player::Rotate(char in){
    if(in == 'a'){          // Rotate counterclockwise
        if(ch == UP)
            ch = LEFT;
        else if(ch == LEFT)
            ch = DOWN;
        else if(ch == DOWN)
            ch = RIGHT;
        else
            ch = UP;
        Print(win);
    } else if(in == 'd'){   // Rotate clockwise
        if(ch == UP)
            ch = RIGHT;
        else if(ch == RIGHT)
            ch = DOWN;
        else if(ch == DOWN)
            ch = LEFT;
        else
            ch = UP;
        Print(win);
    }
}

void Player::GoTo(int x, int y){
	pos.x = x;
	pos.y = y;
}

bool Player::Attack(){
    //Attack point coordinate
    int x,y;   
    if(ch == UP){
        x = pos.x;
        y = pos.y - 1;
    } else if(ch == RIGHT){
        x = pos.x + 1;
        y = pos.y;
    } else if(ch == DOWN){
        x = pos.x;
        y = pos.y + 1;
    } else{
        x = pos.x - 1;
        y = pos.y;
    }

    //Attack to this point
    if((*curMap)[y][x].m_enem != 0){
        int d = GetDamage() + characteristics["damage"];
        int e = (*curMap)[y][x].m_enem->TakeDamage(d );
        text->push_back("Enemy attacked for " + to_string(d));
        //if enemy is died take expirience
        if(e > 0)
            TakeExp(e);
    }
    return true;
}

void Player::TakeDamage(int takeDamage){
    int dam = takeDamage - (GetDefence() + characteristics["defence"]) / 2;
    if(dam < 1)
        dam = 1;
    health -= dam;
    
    if(health < 0){
        health = 0;
        ch = 'X';
    }

    text->push_back("You've been attacked for " + to_string(dam));
}

void Player::Print(WINDOW * win){
    if((*curMap)[pos.y][pos.x].m_obj->GetSym() == 'g'){
        return;
    }
    wattron(win,COLOR_PAIR(PLAYER) | A_BOLD);
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(PLAYER) | A_BOLD);
    wrefresh(win);
}

void Player::SetWinAndEnem(WINDOW * win, vector<vector<Cell>> * curMap, list<string> * text){
    this->win = win;
    this->curMap = curMap;
    this->text = text;
}

int Player::GetStats(string key){
    return characteristics[key];
}

int Player::GetHealth() const{
    return health;
}
int Player::GetDamage() const{
    return inv[curEqpt].damage;
}
int Player::GetDefence() const{
    return inv[curEqpt].defence;
}
int Player::GetExp() const{
    return experience;
}
int Player::GetNextLevelExp() const{
    return nextLevel;
}
int Player::GetCurEqpt() const{
    return curEqpt;
}


void Player::SetExp(int exp){
    experience = exp;
}
void Player::SetNextLevelExp(int nlexp){
    nextLevel = nlexp;
}
void Player::SetHealth(int hlth){
    health = hlth;
}
void Player::SetCurEqpt(int ceqpt){
    curEqpt = ceqpt;
}


void Player::Regen(){
    health = characteristics["health"];
}



void Player::TakeExp(int exp){
    experience += exp;
    text->push_back("Get some exp: " + to_string(exp));

    //If have enough exp then level up
    if(experience >= nextLevel){
        text->push_back("New level!");
        nextLevel *= 2;
        characteristics["health"] += 10;
        characteristics["damage"] += 2;
        characteristics["defence"] += 1;
        characteristics["regeneration"] += 1;
        Regen();
    }
}

Player::~Player(){
}