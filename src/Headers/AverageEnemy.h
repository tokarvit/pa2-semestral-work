#pragma once
#include <ncurses.h>
#include "Constants.h"
#include "Enemy.h"


/*! 
	\brief Class of average enemy.

*/

class AverageEnemy : public Enemy{
public:
	AverageEnemy (Pos p, char ch);
	
	void Print(WINDOW * win);
};
