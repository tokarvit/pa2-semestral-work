# PA1-Semestral-work

This game was made as semestral work with subject PA2 in the year 2017 using C++ and ncurses.
 
The main character was in the army and fought with enemy of him nation.
One day he had woke up and seen a lot of enemies around him. He was on 
the island. The target is get out from it. The various of thing on this 
island can help you with this. 

## Installation
- Step 1: Open folder with game.
- Step 2: Run command "make compile".
- Step 3: Run command "make run".

# Zadani
Naprogramujte engine pro grafické RPG

Váš engine:

ze souboru nahraje definici světa (mapu, předměty, příšery, ...)
umožní vytvořit herní postavu (její atributy a schopnosti) a následnou hru
implementuje jednoduchý soubojový systém a inventář
umožňuje ukládat a načítat rozehrané hry
Engine může fungovat real-time hra, či tahová hra.

# Screenshot
![](Screenshot.jpg)
