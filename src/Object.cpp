#include <ncurses.h>
#include <string>
#include <vector>
#include "Headers/Object.h"
#include "Headers/Constants.h"

using namespace std;

Object::Object(Pos p, char ch) : pos(p) {
    if( ch == 'H' || ch == '1' || ch == '2' || ch == '3' ||
        ch == 'x' || ch == 'y' || ch == 'z')
        this->ch = 's';
    else
        this->ch = ch;
}

char Object::GetSym() const{
    return ch;
} 

int Object::React(){
	if(ch == 'I' || ch == ' ') // Do not move on tree and water
		return NOTRESPASSING;
    else if(ch == 'G') //Next map object
        return NEXTMAP;
	else 
		return NOEFFECT;
}

void Object::Print(WINDOW * win){
    //Different colors for various of objecs
    if(ch == ' '){
        wattron(win,COLOR_PAIR(WATER));
    } else if(ch == 's'){
        wattron(win,COLOR_PAIR(SAND));
    } else if(ch == 'S'){
        wattron(win,COLOR_PAIR(STEM));
    } else if(ch == 'g'){
        wattron(win,COLOR_PAIR(COLOR_GREEN));
    } else if(ch == 'I'){
        wattron(win,COLOR_PAIR(TREE));
    } else if(ch == 'G'){
        wattron(win,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
    } else {
        wattron(win,COLOR_PAIR(COLOR_BLACK));
    }

    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,A_BOLD);
}

Object::~Object(){}