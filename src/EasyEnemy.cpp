#include "Headers/Constants.h"
#include "Headers/EasyEnemy.h"

EasyEnemy::EasyEnemy (Pos p, char ch) : Enemy(p, ch){
	health		= 30;
	damage		= 20;
	endurance	= 1;
	radius		= 4;
	exp			= 50;
}

void EasyEnemy::Print(WINDOW * win){
	wattron(win,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
}