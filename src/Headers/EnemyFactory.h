#pragma once
#include "Constants.h"
#include "Enemy.h"



/*! 
	\brief Factory class for create new enemies.

*/

class EnemyFactory{
public:
	static Enemy * NewEnemy(Pos p, char ch);
};