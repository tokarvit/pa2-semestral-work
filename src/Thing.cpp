#include <ncurses.h>
#include <cmath>
#include "Headers/Constants.h"
#include "Headers/Pos.h"
#include "Headers/Object.h"
#include "Headers/Thing.h"

Thing::Thing(Pos p, char ch, int dam, int def, string name) : Object(p, ch){
	this->ch = ch;
	this->name = name;
	damage = dam;
	defence = def;
}

void Thing::Print(WINDOW * win){
	wattron(win,COLOR_PAIR(THINGC | A_BOLD));
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(THINGC | A_BOLD));
}

Thing * Thing::NewThing(Pos p, char ch){
	if(ch != 'x' && ch != 'y' && ch != 'z'){
		return NULL;
	}
	else {
		int dam, def;
		string name;
		if(ch == 'x'){					//Low level thing
			dam = rand() % 6 + 2;
			def = rand() % 6 + 2;
			if(rand() % 2 == 1)
				name = "Leather armor";
			else
				name = "Dagger";

		} else if(ch == 'y'){			//Middle level thing
			dam = rand() % 10 + 5;
			def = rand() % 10 + 5;
			if(rand() % 2 == 1)
				name = "Iron armor";
			else
				name = "Sword";

		} else if(ch == 'z'){			//High level thing
			dam = rand() % 15 + 10;
			def = rand() % 15 + 10;
			if(rand() % 2 == 1)
				name = "Ebonite armor";
			else
				name = "Long Sword";

		}
		return new Thing(p, ch, dam, def, name);
	}
}