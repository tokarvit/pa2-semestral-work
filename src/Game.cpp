#include <ncurses.h>
#include <vector>
#include <list>
#include <string>

#include "Headers/Constants.h"
#include "Headers/Player.h"
#include "Headers/Func.h"
#include "Headers/Saving.h"
#include "Headers/Game.h"
#include "Headers/Enemy.h"
#include "Headers/Resources.h"
#include "Headers/Thing.h"
#include "Headers/Pos.h"
#include "Headers/Cell.h"


Game::Game(Resources * r){
    sv = new Saving();
    res = r;
    scrWin = newwin(GAME_HEIGHT,GAME_WIDTH-GUI_WIDTH, 0,0);
    GUI    = newwin(GAME_HEIGHT,GUI_WIDTH, 0,GAME_WIDTH-GUI_WIDTH);
    mapWin = NULL;
    haveContinue = false;
    player = NULL;
}

void Game::Play(){
    text.push_back("Game have been started.");
    while(true){
        ShowGame();
        ShowGUI();

        //Player move
        if(!InputsHandle())
            return;       

        if(curMap[player->pos.y][player->pos.x].m_obj->React() == NEXTMAP){
            if(++(sv->currentLevel) >= res->lastMap){
                text.push_back("You win!");
                ShowGUI();
                DialogWin("Congratulation!", "You've done it!", false);
                return;
            }
            MakeMap();
            FillScreen();
            player->SetWinAndEnem(mapWin, &curMap, &text);
            text.push_back("Next map");
            sv->SaveToDisk();
            continue;
        }

        //Enemies move
        for(auto e : enemies){
            if(!e) 
                continue;
            if(e->GetHealth() <= 0){
                e.reset();
            } else{
                e->MakeMove(curMap, player);
            }
        }

        //Check lose level
        if(player->GetHealth() <= 0){
            haveContinue = false;
            text.push_back("You die..");
            ShowGUI();
            player->Print(mapWin);
            wrefresh(mapWin);
            DialogWin("This story have been over...", "But there are still many story ahead!", false);
            return;
        }
    }
}

bool Game::DialogWin(string title, string subtitle, bool isQstn){
    string choice;
    vector <string> choices;

    if(isQstn)
        choices = {"Yes", "No"};
    else
        choices = {"Ok"};

    choice = InteractiveWin(choices, title, subtitle);
    return (isQstn) ? choice == "Yes" : true;  
}

string Game::InteractiveWin(const vector<string> & liststr, string title, string subtitle){
    string choice;
    int w = title.length() + 4;
    if(w < 50)
        w = 50; 
    int h = liststr.size() + 4;
    WINDOW * dialogWin = newwin(h, w,
                    GAME_HEIGHT/2 - h/2,
                    GAME_WIDTH/2 - w/2);
    box(dialogWin, 0 , 0);  

    //Title
    mvwprintw(dialogWin, 1, w/2 - title.length()/2,
            title.c_str());
    mvwprintw(dialogWin, 2, w/2 - subtitle.length()/2,
            subtitle.c_str());
    wrefresh(dialogWin);

    //Get player answer
    choice = SelectMenu(dialogWin, liststr, 3);

    werase(dialogWin);
    wrefresh(dialogWin);    
    delwin(dialogWin); 

    //Update screen
    FillScreen(); 
    ShowGame();
    ShowGUI();

    return choice;
}

bool Game::CallMenu(){
    while(true){
        string choices = ShowMenu();
        if(choices == "New Game"){
            haveContinue = true;
            sv->NewSaving();
            player = sv->player;
            MakeMap();
            FillScreen();
            player->SetWinAndEnem(mapWin, &curMap, &text);
            text.clear();
            return true;
        } else if(choices == "Load"){
            haveContinue = true;
            if(!sv->Load())
                continue;
            player = sv->player;
            MakeMap();
            FillScreen();
            player->SetWinAndEnem(mapWin, &curMap, &text);
            text.clear();
            return true;
        } else if(choices == "Continue"){
            FillScreen();
            return true;
        } else if(choices == "Help"){
            Help();
        } else{
            return false;
        }
    }
}

void Game::ShowGame(){
    for(int y = 0; y < (int)curMap.size(); y++){
        for(int x = 0; x < (int)curMap[y].size(); x++){
            curMap[y][x].Print(mapWin);
        }
    }

    player->Print(mapWin);

    wrefresh(mapWin);
}

void Game::ShowGUI(){
    while(text.size() > 9)
            text.pop_front();

    werase(GUI);
    MakeBox(GUI);

    //Show player face 
    if(player->GetHealth() <= 0)
        res->PrintImg(GUI, "faced", MIDDLE_TOP);
    else if(player->GetHealth() <= player->GetStats("health")/2)
        res->PrintImg(GUI, "faceb", MIDDLE_TOP);
    else
        res->PrintImg(GUI, "faceg", MIDDLE_TOP);

    ShowStats();

    //Show line between logo and stats
    wattron(GUI, COLOR_PAIR(COLOR_WHITE));
    for(int i = 1; i < GUI_WIDTH; i++)
        mvwaddch(GUI, 17, i, ' ');

    //Print log text
    wattron(GUI, COLOR_PAIR(TEXTW));
    int y = 19;
    for(auto t : text){
        mvwprintw(GUI, y++, 2, t.c_str());
        wrefresh(GUI);
    }
    wattroff(GUI, COLOR_PAIR(TEXTW));

    wrefresh(GUI);
}

void Game::ShowStats(){
    wattron(GUI, COLOR_PAIR(TEXTW) | A_BOLD);
    mvwprintw(GUI, 9, 2, "Stats:");

    //Show health
    wattron(GUI, COLOR_PAIR(TEXTR) | A_BOLD);
    mvwprintw(GUI, 10, 2, "Health");
    mvwprintw(GUI, 10, 21, 
            (to_string(player->GetHealth()) + 
            "/" + 
            to_string(player->GetStats("health"))).c_str());

    //Show experience
    wattron(GUI, COLOR_PAIR(TEXTY) | A_BOLD);
    mvwprintw(GUI, 11, 2, "Experience:");
    mvwprintw(GUI, 11, 21, 
            (to_string(player->GetExp()) + 
            "/" + 
            to_string(player->GetNextLevelExp())).c_str());

    //Show damage
    wattron(GUI, COLOR_PAIR(TEXTM) | A_BOLD);
    mvwprintw(GUI, 13, 2, "Damage:");
    mvwprintw(GUI, 13, 21, 
            (to_string(player->GetStats("damage")) + 
            " + " + 
            to_string(player->GetDamage())).c_str());
    //Show defence
    wattron(GUI, COLOR_PAIR(TEXTB) | A_BOLD);
    mvwprintw(GUI, 14, 2, "Defence:");
    mvwprintw(GUI, 14, 21, 
            (to_string(player->GetStats("defence")) + 
            " + " + 
            to_string(player->GetDefence())).c_str());

    //Show regeneration
    wattron(GUI, COLOR_PAIR(TEXTG) | A_BOLD);
    mvwprintw(GUI, 15, 2, "Regen:");
    mvwprintw(GUI, 15, 21, 
            (to_string(player->GetStats("regeneration"))).c_str());
    wattroff(GUI, COLOR_PAIR(TEXTG) | A_BOLD);
}

string Game::ShowMenu(){
    WINDOW * menuWin = newwin(GAME_HEIGHT,GAME_WIDTH, 0,0);
    WINDOW * selectWin = newwin(10,20, GAME_HEIGHT/2, GAME_WIDTH/2 - 10);
    string choice;
    vector <string> choices;

    refresh();
    MakeBox(menuWin);

    wrefresh(menuWin);
    res->PrintImg(menuWin, "logo", MIDDLE_TOP);
    
    //Which option did player choose 
    if(haveContinue)
        choices = {"Continue", "New Game", "Load", "Help", "Exit"};
    else
        choices = {"New Game", "Load", "Help", "Exit"};
    choice = SelectMenu(selectWin, choices, 3);

    werase(selectWin);  
    wrefresh(selectWin);
    delwin(selectWin);
    delwin(menuWin);
    return choice;
}

void Game::MakeMap(){
    ClearEnem();
	string levelName = "map_" + to_string(sv->currentLevel);
    _map = res->GetMap(levelName);

    mapWin = newwin(_map.size(),_map[0].size() - 1,
                    GAME_HEIGHT/2 - _map.size()/2,
                    (GAME_WIDTH - GUI_WIDTH)/2 - _map[0].size()/2);

    //Create map with and all objects we need
    curMap.clear();
	for(int y = 0; y < (int)_map.size(); y++){
		curMap.push_back(vector<Cell>());

		for(int x = 0; x < (int)_map[y].size(); x++){
			char ch = _map[y][x];
            curMap[y].push_back(Cell(Pos(x,y), ch));
            if(curMap[y][x].m_enem)
                enemies.push_back(curMap[y][x].m_enem);
			if(ch == 'H')
				player->GoTo(x,y);
		}
	}
}

void Game::FillScreen(){
    wbkgd(scrWin, COLOR_PAIR(WATER));
    wrefresh(scrWin);
}

bool Game::InputsHandle(){
    while(true){
        int in = getch();
        if(in == 'w' || in == 's' || in == ' '){
            if(player->MakeMove(in))
               break;
        } 
        else if(in == 'a' || in == 'd'){
            player->Rotate(in);
        } 
        else if(in == 'e'){
            //Create invertory window
            vector<string> invText;
            for(auto i: player->inv){
                invText.push_back(i.name + "   damage = " + to_string(i.damage) + 
                                            ", defence = " + to_string(i.defence));
            }
            string res = InteractiveWin(invText, "Inventory");

            //Check which thing was selected
            for(int i = 0; i < (int)invText.size(); i++){
                if(invText[i] == res){
                    player->SetCurEqpt(i);
                    break;
                }
            }

            ShowGUI();
        } else if(in == 27){
            //Exit
            return false;
        }
    }

    return true;
}

void Game::Help(){
    erase();
    refresh();
    WINDOW * boxWin = newwin(GAME_HEIGHT,GAME_WIDTH, 0,0);
    MakeBox(boxWin);
    wrefresh(boxWin);

    int curYpos = 1;
    WINDOW * helpWin = newwin(HELP_HEIGHT, HELP_WIDTH,
                    GAME_HEIGHT/2 - HELP_HEIGHT/2,
                    GAME_WIDTH/2 - HELP_WIDTH/2);
    box(helpWin, 0 , 0);  
    wrefresh(helpWin);

    wattron(helpWin, A_REVERSE);
    mvwprintw(helpWin, curYpos++, HELP_WIDTH/2 - 4,
            "Controls");
    wattroff(helpWin, A_REVERSE);


    mvwprintw(helpWin, curYpos, 2,
            "a,d      -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 15,
            "rotate player");


    mvwprintw(helpWin, curYpos, 2,
            "w,s      -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 13,
            "move player");


    mvwprintw(helpWin, curYpos, 2,
            "space    -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 8,
            "attack");


    mvwprintw(helpWin, curYpos, 2,
            "esc      -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 7,
            "pause");


    mvwprintw(helpWin, curYpos, 2,
            "e        -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 11,
            "inventory");

    //Type of armory help
    mvwprintw(helpWin, curYpos, 2,
            "x,y,z    -");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 8,
            "armory");

    //Enemies help
    wattron(helpWin,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
    mvwprintw(helpWin, curYpos, 2,
            "1");
    wattroff(helpWin,COLOR_PAIR(COLOR_GREEN) | A_BOLD);

    wattron(helpWin,COLOR_PAIR(COLOR_BLUE) | A_BOLD);
    mvwprintw(helpWin, curYpos, 4,
            "2");
    wattroff(helpWin,COLOR_PAIR(COLOR_BLUE) | A_BOLD);

    wattron(helpWin,COLOR_PAIR(COLOR_RED) | A_BOLD);
    mvwprintw(helpWin, curYpos, 6,
            "3");
    wattroff(helpWin,COLOR_PAIR(COLOR_RED) | A_BOLD);
    
    mvwprintw(helpWin, curYpos, 11,
            "-");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 9,
            "enemies");

    //Next level object help
    wattron(helpWin,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
    mvwprintw(helpWin, curYpos, 2,
            "G");
    wattroff(helpWin,COLOR_PAIR(COLOR_GREEN) | A_BOLD);
    mvwprintw(helpWin, curYpos, 11,
            "-");
    mvwprintw(helpWin, curYpos++, HELP_WIDTH - 12,
            "next level");

    wrefresh(helpWin);
    getch();
    werase(helpWin);
    wrefresh(helpWin);
    delwin(helpWin);
}

void Game::ClearEnem(){
    for(auto i : enemies)
        i.reset();

    enemies.clear();
}

Game::~Game(){
    delete sv;
    delwin(scrWin);
    delwin(mapWin);
    delwin(GUI);
}