#pragma once
#include <ncurses.h>
#include "Constants.h"
#include "Pos.h"

/*! 
	\brief Class of one game object.

*/

class Object{
protected:
    char ch;
public:
    Pos pos;

/*! 
	\brief Class.
	\param p Coordinates.
	\param ch Symbol of this object.
*/
    Object(Pos p, char ch);

/*! 
	\brief Get object's symbol .
*/
    char GetSym() const;

/*! 
	\brief Reaction object on other object.
*/
    int React();

/*! 
	\brief Print object to window .
	\param win Window fot print.
*/
    virtual void Print(WINDOW * win);

    virtual ~Object();
};