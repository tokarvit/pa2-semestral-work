# all     create all outputs, which were generated from source code.
# compile create binary of semestral project.
# run     run binary of semestral project.
# clean   reverting to original state of files.
# doc     generate documentation.

CC=g++
CFLAGS=-Wall -pedantic -Wno-long-long -O0 -ggdb -std=c++11
LFLAGS=-lncurses


all: doc compile

tokarvit: AverageEnemy.o Cell.o EasyEnemy.o Enemy.o EnemyFactory.o Func.o Game.o HardEnemy.o main.o Object.o Player.o Pos.o Resources.o Saving.o Thing.o
	$(CC) AverageEnemy.o Cell.o EasyEnemy.o Enemy.o EnemyFactory.o Func.o Game.o HardEnemy.o main.o Object.o Player.o Pos.o Resources.o Saving.o Thing.o -o tokarvit $(LFLAGS)

AverageEnemy.o: src/AverageEnemy.cpp src/Headers/Constants.h src/Headers/AverageEnemy.h src/Headers/Enemy.h src/Headers/Player.h src/Headers/Object.h src/Headers/Pos.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/AverageEnemy.cpp

Cell.o: src/Cell.cpp src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Enemy.h src/Headers/Player.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h src/Headers/EnemyFactory.h
	$(CC) $(CFLAGS) -c src/Cell.cpp

EasyEnemy.o: src/EasyEnemy.cpp src/Headers/Constants.h src/Headers/EasyEnemy.h src/Headers/Pos.h src/Headers/Enemy.h src/Headers/Player.h src/Headers/Object.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/EasyEnemy.cpp

Enemy.o: src/Enemy.cpp src/Headers/Enemy.h src/Headers/Player.h src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/Enemy.cpp

EnemyFactory.o: src/EnemyFactory.cpp src/Headers/Enemy.h src/Headers/Player.h src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h src/Headers/EasyEnemy.h src/Headers/AverageEnemy.h src/Headers/HardEnemy.h src/Headers/EnemyFactory.h
	$(CC) $(CFLAGS) -c src/EnemyFactory.cpp

Func.o: src/Func.cpp src/Headers/Constants.h
	$(CC) $(CFLAGS) -c src/Func.cpp

Game.o: src/Game.cpp src/Headers/Constants.h src/Headers/Player.h src/Headers/Object.h src/Headers/Pos.h src/Headers/Enemy.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h src/Headers/Saving.h src/Headers/Game.h src/Headers/Resources.h
	$(CC) $(CFLAGS) -c src/Game.cpp

HardEnemy.o: src/HardEnemy.cpp src/Headers/Pos.h src/Headers/HardEnemy.h src/Headers/Enemy.h src/Headers/Player.h src/Headers/Object.h src/Headers/Constants.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/HardEnemy.cpp

main.o: src/main.cpp src/Headers/Player.h src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Enemy.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h src/Headers/Saving.h src/Headers/Resources.h src/Headers/Game.h
	$(CC) $(CFLAGS) -c src/main.cpp

Object.o: src/Object.cpp src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h
	$(CC) $(CFLAGS) -c src/Object.cpp

Player.o: src/Player.cpp src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Player.h src/Headers/Enemy.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/Player.cpp

Pos.o: src/Pos.cpp src/Headers/Pos.h
	$(CC) $(CFLAGS) -c src/Pos.cpp

Resources.o: src/Resources.cpp src/Headers/Constants.h src/Headers/Resources.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/Resources.cpp

Saving.o: src/Saving.cpp src/Headers/Saving.h src/Headers/Player.h src/Headers/Object.h src/Headers/Constants.h src/Headers/Pos.h src/Headers/Enemy.h src/Headers/Cell.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/Saving.cpp

Thing.o: src/Thing.cpp src/Headers/Constants.h src/Headers/Pos.h src/Headers/Object.h src/Headers/Thing.h src/Headers/Func.h
	$(CC) $(CFLAGS) -c src/Thing.cpp

doc: config src/Headers/*.h src/main.cpp
	doxygen config

clean:
	rm -f *.o
	rm -f log.txt
	rm -f saving
	rm -f vlg
	rm -f tokarvit
	rm -rf doc

compile: tokarvit

run: tokarvit
	./tokarvit

debug: 
	valgrind --leak-check=full --log-file="vlg" ./tokarvit