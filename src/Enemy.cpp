#include <ncurses.h>
#include <vector>
#include <string>
#include "Headers/Enemy.h"
#include "Headers/Player.h"
#include "Headers/Constants.h"
#include "Headers/Pos.h"
#include "Headers/Func.h"
#include "Headers/Cell.h"




Enemy::Enemy(Pos p, char ch) : Object(p, ch){
    this->ch = ch;
}

void Enemy::MakeMove(vector<vector<Cell>> & map, Player * pl){
    int en = endurance; // How many moves he has
    int dist;

    //Move untill endurance is not end.
    while(en > 0){
        int x = pos.x;
        int y = pos.y;
        dist = abs(pl->pos.x - pos.x) + abs(pl->pos.y - pos.y); //distance to player

        //If player is near attack him
        if( dist == 1 ){
            pl->TakeDamage(damage);
            en--;
            continue;
        } 

        //Find the way to player
        if( dist < radius ){
            if(pl->pos.x < x){
                x--;
                if(!map[y][x].CanMoveHere()){
                    x++;
                    if(pl->pos.y < y){
                        y--;
                    } else if(pl->pos.y > y){
                        y++;
                    }
                }
            } else if(pl->pos.x > x){
                x++;
                if(!map[y][x].CanMoveHere()){
                    x--;
                    if(pl->pos.y < y){
                        y--;
                    } else if(pl->pos.y > y){
                        y++;
                    }
                }
            } else if(pl->pos.x == x){
                if(pl->pos.y < y){
                    y--;
                } else if(pl->pos.y > y){
                    y++;
                }
            }
        }

        //If i can go on new place then i will go
        if(map[y][x].CanMoveHere()){
            map[y][x].m_enem = move(map[pos.y][pos.x].m_enem);
            pos.x = x;
            pos.y = y;
        }

        en--;
    }
}

int Enemy::TakeDamage(int takeDamage){
    health -= takeDamage;
    if(health <= 0)
        return exp;
    else
        return 0;
}

void Enemy::Print(WINDOW * win){
	wattron(win,COLOR_PAIR(COLOR_RED) | A_BOLD);
    mvwaddch(win, pos.y, pos.x, ch);
    wattroff(win,COLOR_PAIR(COLOR_RED) | A_BOLD);
}

int Enemy::GetHealth(){
    return health;
}