#pragma once
#include <ncurses.h>
#include <vector>
#include <string>
#include <list>
#include "Player.h"
#include "Saving.h"
#include "Cell.h"
#include "Resources.h"
#include "Enemy.h"

/*! 
    \brief The main class of the game.
*/

class Game{
    vector<vector<Cell>> curMap;    //< Current game's map.
    list<shared_ptr<Enemy>> enemies;//< List of enemies.
    vector<string> _map;            //< The pristine copy of current map.
    Player * player;                //< Pointer to player .
    WINDOW * mapWin;                //< The map window.
    WINDOW * scrWin;                //< The main window.
    WINDOW * GUI;                   //< The UI window.
    Resources * res;                //< Pointer to resources.
    list<string> text;              //< Log, which is visible in the UI window.
    bool haveContinue;              //< Can game continue?.
    Saving * sv;                    //< Pointer to saving, which consists informaion about current game and player.

    void ShowGame();
    
    void ShowGUI();

/*! 
    \brief Create map with all enemies, player etc.
*/
    void MakeMap();

    void Help();

    bool DialogWin(string title, string subtitle, bool isQstn = true);
    
    string InteractiveWin(const vector<string> & liststr, string title, string subtitle = "");
/*! 
    \brief Show player's stats in GUI window.
*/
    void ShowStats();

    bool InputsHandle();

/*! 
    \brief Delete all enemies.
*/
    void ClearEnem();

/*! 
    \brief Fiil mapWin with water.
*/
    void FillScreen();

/*! 
    \brief Call main menu.
*/
    string ShowMenu();

public:
    Game(Resources * r);

/*! 
    \brief Processing player choice in main menu.
*/
    bool CallMenu();

/*! 
  \brief Method include the main game's loop.
*/
    void Play();

    ~Game();
};