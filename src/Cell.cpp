#include <memory>
#include "Headers/Object.h"
#include "Headers/Enemy.h"
#include "Headers/Thing.h"
#include "Headers/Cell.h"
#include "Headers/Pos.h"
#include "Headers/EnemyFactory.h"



Cell::Cell(Pos p, char ch) {
	m_obj.reset(new Object(p, ch));
	m_enem.reset(EnemyFactory::NewEnemy(p, ch));
	m_th.reset(Thing::NewThing(p, ch));
}

void Cell::Print(WINDOW * win){
	if(m_enem && m_enem->GetHealth() <= 0)
		m_enem.reset();

	m_obj->Print(win);

	//if object is tree then dont show anything above it.
	if(m_obj->GetSym() == 'g')
		return;
	
	if(m_th)
		m_th->Print(win);

	if(m_enem)
		m_enem->Print(win);
}

bool Cell::CanMoveHere(){
	if(m_enem){
    	return false;
	} else if(m_obj->React() == NOTRESPASSING)
        return false;

    return true;
}