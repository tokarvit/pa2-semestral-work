#pragma once
#include "Player.h"
#include "Constants.h"

/*! 
    \brief Class with game progress.
*/
class Saving{
public:
    Player * player;
    int currentLevel;

    Saving();

/*! 
    \brief New history and player.
*/
    void NewSaving();

/*! 
    \brief Read saving file from disk.
*/
    bool Load();
    
/*! 
    \brief Save progress to saving file.
*/
    bool SaveToDisk();


    ~Saving();
};