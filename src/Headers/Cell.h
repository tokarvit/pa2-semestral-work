#pragma once
#include <memory>
#include "Object.h"
#include "Enemy.h"
#include "Thing.h"
#include "Pos.h"

class Enemy;

/*! 
	\brief Class of one specific console pixel.

*/


class Cell{

public:
	shared_ptr<Object> m_obj;///< Object of ground. For example water, grass.
	shared_ptr<Enemy>  m_enem;///< Object of enemy.
	shared_ptr<Thing>  m_th;///< Object of thing, clothes.

/*!
	\brief Class's constructor.
	\param p Object's position.
	\param ch Object's symbol.
*/
	Cell(Pos p, char ch);

/*!
	\brief Print object to window.
*/
	void Print(WINDOW * win);

/*!
	\brief Can someone move to this position?.
*/
	bool CanMoveHere();
};